# APXploit
Note: this project is not useful without a working Fusée Gelée.

> Do you want your platform firmware audited for free? Put it in a game console 
>
>  -- Some wise hacker whose name I forgot

Tool for temporary root with Dirty CoW and rebooting into APX mode using a single command, for the Sony Tablet S (also known as S1, codename nbx03).

Sony did not made the bootloader of the Tablet S unlockable. This device has a Tegra 20, and the Fusée Gelée exploit is not yet compatible with this SoC as of September 2023. If this will ever appear in the future, you can combine this exploit to replace the bootloader with u-boot for example. 

## Dependencies
* GNU Make
* Android Debug Bridge
* Git

## Usage
* Clone this repo
* Make sure ADB is enabled on the tablet and make sure it is connected (some long number should be shown when running `adb devices`)
* `make run`

If it succeeded, you should see something with "Product: APX" in your `dmesg`:
```
[76806.930794] usb 1-2: new high-speed USB device number 34 using xhci_hcd
[76807.057705] usb 1-2: New USB device found, idVendor=0955, idProduct=7420, bcdDevice= 1.04
[76807.057724] usb 1-2: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[76807.057732] usb 1-2: Product: APX
[76807.057738] usb 1-2: Manufacturer: NVIDIA Corp.
```

## Development
Compiling this requires an old version of the Android NDK that still supports old platforms.

https://github.com/android/ndk/wiki/Changelog-r15

Download it here: https://github.com/android/ndk/wiki/Unsupported-Downloads#r15c

Add the folder which contains the binaries to your PATH variable.

I could not find which version of the NDK dropped support for Ice Cream Sandwich, but the last NDK that explicitly supported it was version 15.

Merge requests are very welcome. I would like to make this exploit more compatible with other hardware in the future. ADB over WebUSB would be really cool.

## Credits
* People in the postmarketOS on Tegra group
* The BusyBox project
* Dirty CoW developers, specifically this project: https://github.com/timwr/CVE-2016-5195 (see license: https://github.com/timwr/CVE-2016-5195/issues/103)
* Kate Temkin for the Fusée Gelée exploit, without it this project would be useless

## Discredits
* Sony Corporation, for not making an unlockable bootloader without needing to resort to exploits

## More info
The latest Android update released from Sony is Android 4.0.3, which equals platform API version 15. It does not have SELinux, which saves another bypass.

It also doesn't seem to be a particular well built tablet. The display gets issues after some years, which can be seen on various eBay pictures. Although some of them survived fine. Might have to do something with Sony their TruBlack Display technology. It might be fixable if removing top layer from the panel, which might destroy the panel as well. However, even without the panel it would still be usable as a general purpose Linux box with a mainline kernel.

Compiling the Dirty CoW code is done with NDK (native development kit) version R15, because newer versions have dropped support for this platform.

APX mode is a boot ROM mode of Nvidia Tegra SoCs that for low-level access. Using the Fusée Gelée exploit, it can be used for booting into arbitrary code. Other Tegra devices usually added a feature to the bootloader for entering APX mode using a key combination. To me it is unclear if Sony implemented this. Some people on XDA Developers claimed to have accessed APX using a shortcut the power and a volume button, but I was unable to replicate it. A lot of other users also claimed it did not work. I don't know what is happening, but at least this exploit worked reliable during my testing efforts. This exploit works by writing two bytes into memory in the OS using privilileged access using Dirty CoW.

I have no idea what APX means, it seems that Nvidia inherited the name from the PortalPlayer APX when they acquired PortalPlayer LLC in 2007 and then continued with the name for their Tegra SoCs.

For more information on the device, see the postmarketOS wiki: https://wiki.postmarketos.org/wiki/Sony_Tablet_S_(sony-nbx03)

Spec sheet: https://specdevice.com/showspec.php?id=19b8-de6e-0033-c5870033c587
